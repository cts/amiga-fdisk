#define VOID		void

#ifndef __alpha__
typedef long		LONG;	    /* signed 32-bit quantity */
typedef unsigned long	ULONG;	    /* unsigned 32-bit quantity */
typedef unsigned long	LONGBITS;   /* 32 bits manipulated individually */
#else
typedef int		LONG;	    /* signed 32-bit quantity */
typedef unsigned int	ULONG;	    /* unsigned 32-bit quantity */
typedef unsigned int	LONGBITS;   /* 32 bits manipulated individually */
#endif

typedef short		WORD;	    /* signed 16-bit quantity */
typedef unsigned short	UWORD;	    /* unsigned 16-bit quantity */
typedef unsigned short	WORDBITS;   /* 16 bits manipulated individually */
#if __STDC__
typedef signed char	BYTE;	    /* signed 8-bit quantity */
#else
typedef char		BYTE;	    /* signed 8-bit quantity */
#endif
typedef unsigned char	UBYTE;	    /* unsigned 8-bit quantity */
typedef unsigned char	BYTEBITS;   /* 8 bits manipulated individually */
typedef unsigned short	RPTR;	    /* signed relative pointer */
typedef unsigned char  *STRPTR;     /* string pointer (NULL terminated) */

/* Types with specific semantics */
typedef float		FLOAT;
typedef double		DOUBLE;
typedef short		BOOL;
typedef unsigned char	TEXT;

#ifndef TRUE
#define TRUE		1
#endif
#ifndef FALSE
#define FALSE		0
#endif
#ifndef NULL
#define NULL		0L
#endif


#define BYTEMASK	0xFF


