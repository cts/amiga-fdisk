struct notimplemented {
	char		key;
	char		*desc;
};

const struct notimplemented functions[] =
{
	{ 's',	"List size of the partition" },
	{ 'd',	"Dump partition table in amiga-fdisk format" },
	{ 'V',	"Verify partitions" },
	{ 'i',	"Start with cylinder 1 instead of 0" },
	{ 'N',	"Change only single partition" },
	{ 'A',	"Make partition active" },
	{ 'c',	"Change partition id" },
	{ 'u',	"Unit size" },
	{ 'x',	"Show extended" },
	{ 'C',	"Specify Cylinders - override kernel" },
	{ 'H',	"Specify Heads- override kernel" },
	{ 'S',	"Specify Sectors- override kernel" },
	{ 'f',	"Force - even if stupid" },
	{ 'q',	"Quiet execution" },
	{ 'L',	"Not complain about Linux irrelevant things" },
	{ 'n',	"Do not write to disk" },
	{ 'O',	"Save Sector Table to file" },
	{ 'I',	"Restore Sector Table from file" }
};
