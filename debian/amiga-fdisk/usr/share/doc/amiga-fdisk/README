amiga-fdisk 0.04 (consider this being unstable)

This is an fdisk program for partioning harddrives using the Amiga
Rigid Disk Block standard(RDB) It's in a very early stage.

The most current release of amiga-fdisk can be obtained from:
                  http://www.freiburg.linux.de/~stepan

Amiga-fdisk is known to work on Linux systems with at least all kernels
since 1.2.13. I suggest that you use at least a recent 2.0 kernel.

Be careful! With amiga-fdisk, it is easy to erase your whole
data within few seconds

If you have a MBR on your harddisk, too, you should *NEVER* reorganize rdb to
block 0  (or 1?) or the MBR gets lost. If you don't want to read your harddisks
in a PC style workstation, this is very likely of no interest to you.

Thanks to Frank Neumann and Leland for their ideas and help.
Many thanks to Joerg Dorchain. He made the first attempt to write an
amiga-fdisk and he allowed me to have a *very* close look at his code.
In fact, I think some larger parts are stolen from his work.


Look at the TODO-file if you feel inspired to work on amiga-fdisk.  Feel
free to ask me anything about the RDB interna.

Please note: This program has been developed on an Intel box running
Linux/x86, but I have tested it on my Amiga 3000 running Linux m68k
quite often now. It works fine for me and it seems that it behaves
equally on both little and big endianed platforms.

This version is still ALPHA. It's to your responsibility
if your data gets lost. (I am sorry for this, but of course I will try
to help you, if your harddisks crash because of amiga-fdisk)

For the Dostype-Choice, read the following:

Cite from 'The Amiga Guru Book' from Ralph Babel, Page 561:

de_DosType - The DOS type determines a disk's block structure; $444F5300
  ('DOS\0')identifies the old filesystem (default), $444F5301 ('DOS\1') the
  FastFileSystem, and $444F5302 ('DOS\2') and $444F5303 ('DOS\3') the new
  "international" versions thereof. It is used as the default DOS type by
  Format, as the 2.0 ROM filesystem is able to handle different formats
  (see section 17.1.50). The filesystem-private DOS-type identifier as
  stored in block 0 of a partition determines the actual block structure
  (see section 15.3.9).

  For DosEnvec structures that are part of the RDB, de_DosType is also used
  to indicate the type of partition data, e.g under Amiga-UNIX ( :-) ):

  UNI\0 - classic AT&T System-V filesystem
  UNI\1 - UNIX boot "filesystem" (dummy entry for Amiga OS's boot menu)
  UNI\2 - Berkeley filesystem for System V
  RESV  - reserved (e.g. swap space)

  Under 2.0, FileSystem.resource entries exist for DOS\1, DOS\2, DOS\3 and
  UNI\1 (i.e all bootable "filesystems" other than DOS\0, which is always
  considered to be available as boot filesystem). As different revisions of
  Commodore's filesystems can handle different sets of partition formats,
  it is unclear wether de_DosType is used as an alias for the filesystem
  code to be used (e.g. during autobooting) or as an indicator as to which
  format the partition is in. Neither of these interpretations is fully
  consistent with the current usage and existing filesystems and may
  therefore cause problems.

What follows up from this for now is:

If you set the DosType of all your Linux partitions to RESV and Flags
to noboot,nomount, they will not appear in the boot menu nor 
show up anyhow under Amiga OS.

Set the DosType to RESV and Flags to noboot,mount if you want to share a
partition (e.g swap) between Linux and AmigaDos without an appropriate
filesystem (e.g. for use with VMM). This gives only the device name
without a volume node -> no annoying "Not a Dos disk" icon.
set the DosType to UNI\1, Flags to boot,mount (priority as desired) and
BootBlocks to 2 of your Linux root partition for use of Amiga-LILO.
So the partion will show up in the bootmenu but dissappear silently
when booting AmigaDos from another partition.


	Stefan Reinauer, <stepan@linux.de>

November 1998.
